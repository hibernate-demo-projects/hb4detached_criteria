package sh.main;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import sh.daos.BookDao;
import sh.entities.Book;
import sh.util.HbUtil;

public class Hb4Main {

	public static void main(String[] args) {
		BookDao bookDao = new BookDao();

		/*
		 * Transaction is compulsory if session object is obtained from
		 * getCurrentSessionM() method.
		 */
		try {
			HbUtil.beginTransaction();

			/*
			 * Create detached criteria
			 */
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Book.class);
			
			/*
			 * Add restrictions in detached criteria
			 */
			detachedCriteria
					.add(Restrictions.or(Restrictions.eq("subject", "os"), Restrictions.eq("author", "SCHILDT")));
			
			List<Book> books = bookDao.findByDetachedCriteria(detachedCriteria);

			HbUtil.commitTransaction();
			
			books.forEach(System.out::println);

		} catch (Exception e) {
			HbUtil.rollbackTransaction();
			e.printStackTrace();
		}
	}
}
